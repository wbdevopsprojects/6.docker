
NodeJS
-----------------
-   Install nodejs
    
    curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash - &&\
    sudo apt-get install -y nodejs
    node --version

-   mkdir nodejs && cd nodejs
-   nano node.js

    const http = require('node:http');

    const hostname = '0.0.0.0';
    const port = 3000;

    const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello from NodeJS!\n');
    });

    server.listen(port, hostname, () => { 
    console.log(`Server running at http://${hostname}:${port}/`);
    });         

-   nano Dockerfile

    FROM node:18-alpine
    WORKDIR /app
    COPY ./node.js ./node.js
    EXPOSE 3000
    CMD ["node","node.js"]


Express JS
------------------------------------
-   mkdir express && cd express
-   nano express.js

    const express = require('express')
    const app = express()
    const port = 3000

    app.get('/', (req, res) => {
    res.send('<h2>Hello from Express!</h2>')
    })

    app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
    })

-   npm init -y
-   npm install express
-   node express.js



